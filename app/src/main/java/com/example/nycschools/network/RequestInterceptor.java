package com.example.nycschools.network;

import androidx.annotation.NonNull;

import com.example.nycschools.utils.NetworkCheck;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public class RequestInterceptor implements Interceptor {

    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!NetworkCheck.isNetworkAvailable()) {
            throw new OfflineException();
        }

        return chain.proceed(chain.request().newBuilder().build());
    }

    public  class OfflineException extends IOException {

        public String getErrorMessage() {
            return "No network available, please check your WiFi or Data connection";
        }
    }
}