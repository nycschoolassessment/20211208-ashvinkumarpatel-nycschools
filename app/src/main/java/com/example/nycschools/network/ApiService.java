package com.example.nycschools.network;

import com.example.nycschools.model.SchoolScoreInfo;
import com.example.nycschools.model.SchoolInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    String BASE_URL = "https://data.cityofnewyork.us/";

    @GET("resource/s3k6-pzi2.json")
    Call<List<SchoolInfo>> getListOfNYCSchools();

    @GET("/resource/f9bf-2cp4.json")
    Call<List<SchoolScoreInfo>> getDataOfSchools();

}