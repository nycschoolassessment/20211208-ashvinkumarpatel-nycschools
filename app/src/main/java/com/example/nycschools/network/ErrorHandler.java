package com.example.nycschools.network;


import com.example.nycschools.utils.GlobalVariables;

public class ErrorHandler {

    public static void handleRetrofitError(Throwable t) {
        if (t instanceof RequestInterceptor.OfflineException) {
            GlobalVariables.toastMessage.postValue(((RequestInterceptor.OfflineException) t).getErrorMessage());
            return;
        }

        GlobalVariables.toastMessage.postValue("An error has occured");
    }
}
