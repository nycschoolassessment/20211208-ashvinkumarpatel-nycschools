package com.example.nycschools.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolInfo;
import com.example.nycschools.ui.adapter.SchoolListAdapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NycScoolslistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NycScoolslistFragment extends Fragment {

    private SharedViewModel sharedViewModel;
    private SchoolListAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    public NycScoolslistFragment() {
        // Required empty public constructor
    }

    public static NycScoolslistFragment newInstance(String param1, String param2) {
        NycScoolslistFragment fragment = new NycScoolslistFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        sharedViewModel.init();




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_nyc_scools_list, container, false);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mRecyclerView = view.findViewById(R.id.recyclerview);
        initRecyclerView();

        mSwipeRefreshLayout.setRefreshing(true);
        sharedViewModel.getSchoolsInfo().observe(getActivity(), new Observer<List<SchoolInfo>>() {
            @Override
            public void onChanged(List<SchoolInfo> schoolInfos) {
                mSwipeRefreshLayout.setRefreshing(false);
                if(schoolInfos != null) {
                    mAdapter.setResult(schoolInfos);
                }
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                sharedViewModel.refreshSchoolsInfo().observe(getActivity(), new Observer<List<SchoolInfo>>() {
                    @Override
                    public void onChanged(List<SchoolInfo> schoolInfos) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if(schoolInfos != null) {
                            mAdapter.setResult(schoolInfos);
                        }
                    }
                });
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    private void initRecyclerView() {
        mAdapter = new SchoolListAdapter(this, sharedViewModel.getSchoolsInfo().getValue());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);

    }

}