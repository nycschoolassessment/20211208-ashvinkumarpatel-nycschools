package com.example.nycschools.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.nycschools.data.SchoolRepository;
import com.example.nycschools.model.SchoolScoreInfo;
import com.example.nycschools.model.SchoolInfo;

import java.util.List;

public class SharedViewModel extends ViewModel {
    private SchoolRepository mRepository;
    private MutableLiveData<List<SchoolInfo>> mSchoolsInfo;
    private MutableLiveData<List<SchoolScoreInfo>> responseItemMutableLiveData ;

    public void init() {
        if(mSchoolsInfo != null) {
            return;
        }
        mRepository = SchoolRepository.getInstance();
        mSchoolsInfo = mRepository.getListOfSchoolInfo();
    }

    public LiveData<List<SchoolInfo>> getSchoolsInfo() {
        return mSchoolsInfo;
    }

    public LiveData<List<SchoolInfo>> refreshSchoolsInfo() {
        mRepository.getListOfSchoolInfo();
        return mSchoolsInfo;
    }

    public LiveData<List<SchoolScoreInfo>> getSATResultOfSchool() {
        if(responseItemMutableLiveData != null) {
            return responseItemMutableLiveData;
        }
        responseItemMutableLiveData = mRepository.getSATResultOfSchool();
        return responseItemMutableLiveData;
    }


}
