package com.example.nycschools.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolScoreInfo;

import java.util.List;

public class SchoolScoreInfoFragment extends Fragment {

    private final String KEY_SCHOOL_NAME = "schoolName";
    private SharedViewModel mSharedViewModel;
    private String mSchoolName;



    public SchoolScoreInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null) {
            mSchoolName = bundle.getString(KEY_SCHOOL_NAME);
        }

        mSharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        mSharedViewModel.init();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_school_score_info, container, false);

        TextView satScoreTextView = view.findViewById(R.id.satScoreTextView);
        TextView writingScoreTextView = view.findViewById(R.id.writingScoreTextView);
        TextView readingScoreTextView = view.findViewById(R.id.readingScoreTextView);
        TextView mathScoreTextView = view.findViewById(R.id.mathScoreTextView);


        mSharedViewModel.getSATResultOfSchool().observe(getActivity(), new Observer<List<SchoolScoreInfo>>() {
            @Override
            public void onChanged(List<SchoolScoreInfo> schoolScoreInfos) {

                if (schoolScoreInfos != null) {
                    for (SchoolScoreInfo schoolScoreInfo : schoolScoreInfos) {
                        if (schoolScoreInfo.getSchoolName().equalsIgnoreCase(mSchoolName)) {
                            satScoreTextView.setText(schoolScoreInfo.getNumOfSatTestTakers());
                            mathScoreTextView.setText(schoolScoreInfo.getSatMathAvgScore());
                            readingScoreTextView.setText(schoolScoreInfo.getSatWritingAvgScore());
                            writingScoreTextView.setText(schoolScoreInfo.getSatWritingAvgScore());
                        }
                    }
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }
}