package com.example.nycschools.ui.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.nycschools.R;
import com.example.nycschools.model.SchoolInfo;
import com.example.nycschools.ui.NycScoolslistFragment;

import java.util.ArrayList;
import java.util.List;

public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolListHolder> {

    private final String KEY_SCHOOL_NAME = "schoolName";
    private List<SchoolInfo> mSchoolInfoList = new ArrayList<>();

    public SchoolListAdapter(NycScoolslistFragment nycScoolslistFragment, List<SchoolInfo> schoolInfoList) {
        mSchoolInfoList = schoolInfoList;
    }

    @NonNull
    @Override
    public SchoolListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_school, parent, false);
        return new SchoolListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolListHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mSchoolInfoList != null ? mSchoolInfoList.size() : 0   ;
    }

    public void setResult(List<SchoolInfo> schoolInfoResponse) {
        mSchoolInfoList = schoolInfoResponse;
        notifyDataSetChanged();
    }

    public void clearData() {
        mSchoolInfoList = null;
    }

    public class SchoolListHolder extends RecyclerView.ViewHolder {

        private TextView schoolNameTextView;
        private TextView schoolWebsiteUrl;
        private TextView schoolAddressTextView;

        public SchoolListHolder(View view) {
            super(view);
            schoolNameTextView = view.findViewById(R.id.schoolName);
            schoolWebsiteUrl = view.findViewById(R.id.schoolWebsite);
            schoolAddressTextView = view.findViewById(R.id.schoolAddress);
        }

        private void bind(int position) {

            SchoolInfo schoolInfo = mSchoolInfoList.get(position);

            schoolNameTextView.setText(schoolInfo.getSchoolName());
            schoolWebsiteUrl.setText(schoolInfo.getWebsite());
            schoolAddressTextView.setText(schoolInfo.getPrimaryAddressLine1());

            itemView.findViewById(R.id.cvRow).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString(KEY_SCHOOL_NAME, schoolInfo.getSchoolName());
                    Navigation.findNavController(view).navigate(R.id.action_nycScoolslistFragment_to_schoolResultFragment, bundle);
                }
            });
        }
    }
}
