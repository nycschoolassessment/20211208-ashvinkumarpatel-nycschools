package com.example.nycschools.model;

import com.squareup.moshi.Json;

public class SchoolScoreInfo {

	@Json(name = "dbn")
	private String dbn;

	@Json(name = "sat_writing_avg_score")
	private String satWritingAvgScore;

	@Json(name = "sat_critical_reading_avg_score")
	private String satCriticalReadingAvgScore;

	@Json(name = "sat_math_avg_score")
	private String satMathAvgScore;

	@Json(name = "school_name")
	private String schoolName;

	@Json(name = "num_of_sat_test_takers")
	private String numOfSatTestTakers;

	public String getDbn(){
		return dbn;
	}

	public String getSatWritingAvgScore(){
		return satWritingAvgScore;
	}

	public String getSatCriticalReadingAvgScore(){
		return satCriticalReadingAvgScore;
	}

	public String getSatMathAvgScore(){
		return satMathAvgScore;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public String getNumOfSatTestTakers(){
		return numOfSatTestTakers;
	}
}