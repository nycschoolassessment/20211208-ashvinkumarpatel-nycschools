package com.example.nycschools.model;

import com.squareup.moshi.Json;

public class SchoolInfo {

	@Json(name = "bus")
	private String bus;

	@Json(name = "grade9gefilledflag1")
	private String grade9gefilledflag1;

	@Json(name = "method1")
	private String method1;

	@Json(name = "method2")
	private String method2;

	@Json(name = "grade9gefilledflag2")
	private String grade9gefilledflag2;

	@Json(name = "bin")
	private String bin;

	@Json(name = "borough")
	private String borough;

	@Json(name = "grade9geapplicantsperseat1")
	private String grade9geapplicantsperseat1;

	@Json(name = "grade9geapplicantsperseat2")
	private String grade9geapplicantsperseat2;

	@Json(name = "ell_programs")
	private String ellPrograms;

	@Json(name = "diplomaendorsements")
	private String diplomaendorsements;

	@Json(name = "psal_sports_girls")
	private String psalSportsGirls;

	@Json(name = "academicopportunities1")
	private String academicopportunities1;

	@Json(name = "academicopportunities2")
	private String academicopportunities2;

	@Json(name = "academicopportunities3")
	private String academicopportunities3;

	@Json(name = "academicopportunities4")
	private String academicopportunities4;

	@Json(name = "academicopportunities5")
	private String academicopportunities5;

	@Json(name = "total_students")
	private String totalStudents;

	@Json(name = "psal_sports_coed")
	private String psalSportsCoed;

	@Json(name = "longitude")
	private String longitude;

	@Json(name = "zip")
	private String zip;

	@Json(name = "finalgrades")
	private String finalgrades;

	@Json(name = "seats9swd2")
	private String seats9swd2;

	@Json(name = "admissionspriority11")
	private String admissionspriority11;

	@Json(name = "seats9swd1")
	private String seats9swd1;

	@Json(name = "admissionspriority12")
	private String admissionspriority12;

	@Json(name = "fax_number")
	private String faxNumber;

	@Json(name = "bbl")
	private String bbl;

	@Json(name = "dbn")
	private String dbn;

	@Json(name = "start_time")
	private String startTime;

	@Json(name = "pct_stu_safe")
	private String pctStuSafe;

	@Json(name = "pct_stu_enough_variety")
	private String pctStuEnoughVariety;

	@Json(name = "interest1")
	private String interest1;

	@Json(name = "overview_paragraph")
	private String overviewParagraph;

	@Json(name = "school_accessibility_description")
	private String schoolAccessibilityDescription;

	@Json(name = "community_board")
	private String communityBoard;

	@Json(name = "language_classes")
	private String languageClasses;

	@Json(name = "phone_number")
	private String phoneNumber;

	@Json(name = "neighborhood")
	private String neighborhood;

	@Json(name = "campus_name")
	private String campusName;

	@Json(name = "grade9geapplicants2")
	private String grade9geapplicants2;

	@Json(name = "grade9swdfilledflag1")
	private String grade9swdfilledflag1;

	@Json(name = "state_code")
	private String stateCode;

	@Json(name = "council_district")
	private String councilDistrict;

	@Json(name = "interest2")
	private String interest2;

	@Json(name = "grade9geapplicants1")
	private String grade9geapplicants1;

	@Json(name = "seats101")
	private String seats101;

	@Json(name = "grade9swdapplicantsperseat1")
	private String grade9swdapplicantsperseat1;

	@Json(name = "grade9swdfilledflag2")
	private String grade9swdfilledflag2;

	@Json(name = "seats102")
	private String seats102;

	@Json(name = "grade9swdapplicantsperseat2")
	private String grade9swdapplicantsperseat2;

	@Json(name = "shared_space")
	private String sharedSpace;

	@Json(name = "prgdesc2")
	private String prgdesc2;

	@Json(name = "code2")
	private String code2;

	@Json(name = "code1")
	private String code1;

	@Json(name = "prgdesc1")
	private String prgdesc1;

	@Json(name = "city")
	private String city;

	@Json(name = "latitude")
	private String latitude;

	@Json(name = "building_code")
	private String buildingCode;

	@Json(name = "graduation_rate")
	private String graduationRate;

	@Json(name = "nta")
	private String nta;

	@Json(name = "extracurricular_activities")
	private String extracurricularActivities;

	@Json(name = "census_tract")
	private String censusTract;

	@Json(name = "website")
	private String website;

	@Json(name = "boro")
	private String boro;

	@Json(name = "end_time")
	private String endTime;

	@Json(name = "school_name")
	private String schoolName;

	@Json(name = "grades2018")
	private String grades2018;

	@Json(name = "primary_address_line_1")
	private String primaryAddressLine1;

	@Json(name = "psal_sports_boys")
	private String psalSportsBoys;

	@Json(name = "school_email")
	private String schoolEmail;

	@Json(name = "subway")
	private String subway;

	@Json(name = "school_10th_seats")
	private String school10thSeats;

	@Json(name = "grade9swdapplicants2")
	private String grade9swdapplicants2;

	@Json(name = "grade9swdapplicants1")
	private String grade9swdapplicants1;

	@Json(name = "attendance_rate")
	private String attendanceRate;

	@Json(name = "program1")
	private String program1;

	@Json(name = "location")
	private String location;

	@Json(name = "program2")
	private String program2;

	@Json(name = "seats9ge2")
	private String seats9ge2;

	@Json(name = "seats9ge1")
	private String seats9ge1;

	@Json(name = "advancedplacement_courses")
	private String advancedplacementCourses;

	@Json(name = "college_career_rate")
	private String collegeCareerRate;

	@Json(name = "admissionspriority21")
	private String admissionspriority21;

	@Json(name = "addtl_info1")
	private String addtlInfo1;

	@Json(name = "admissionspriority51")
	private String admissionspriority51;

	@Json(name = "school_sports")
	private String schoolSports;

	@Json(name = "offer_rate1")
	private String offerRate1;

	@Json(name = "admissionspriority41")
	private String admissionspriority41;

	@Json(name = "admissionspriority31")
	private String admissionspriority31;

	@Json(name = "pbat")
	private String pbat;

	@Json(name = "international")
	private String international;

	@Json(name = "eligibility1")
	private String eligibility1;

	@Json(name = "eligibility2")
	private String eligibility2;

	@Json(name = "requirement2_1")
	private String requirement21;

	@Json(name = "requirement4_1")
	private String requirement41;

	@Json(name = "requirement6_1")
	private String requirement61;

	@Json(name = "directions1")
	private String directions1;

	@Json(name = "requirement1_1")
	private String requirement11;

	@Json(name = "requirement3_1")
	private String requirement31;

	@Json(name = "requirement5_1")
	private String requirement51;

	@Json(name = "boys")
	private String boys;

	@Json(name = "admissionspriority23")
	private String admissionspriority23;

	@Json(name = "seats9swd6")
	private String seats9swd6;

	@Json(name = "seats9swd5")
	private String seats9swd5;

	@Json(name = "seats9swd4")
	private String seats9swd4;

	@Json(name = "seats9swd3")
	private String seats9swd3;

	@Json(name = "offer_rate3")
	private String offerRate3;

	@Json(name = "grade9swdfilledflag5")
	private String grade9swdfilledflag5;

	@Json(name = "grade9swdfilledflag4")
	private String grade9swdfilledflag4;

	@Json(name = "seats105")
	private String seats105;

	@Json(name = "grade9swdfilledflag6")
	private String grade9swdfilledflag6;

	@Json(name = "seats106")
	private String seats106;

	@Json(name = "grade9geapplicants6")
	private String grade9geapplicants6;

	@Json(name = "grade9geapplicants5")
	private String grade9geapplicants5;

	@Json(name = "admissionspriority16")
	private String admissionspriority16;

	@Json(name = "admissionspriority13")
	private String admissionspriority13;

	@Json(name = "seats103")
	private String seats103;

	@Json(name = "seats104")
	private String seats104;

	@Json(name = "prgdesc6")
	private String prgdesc6;

	@Json(name = "grade9geapplicants4")
	private String grade9geapplicants4;

	@Json(name = "grade9swdfilledflag3")
	private String grade9swdfilledflag3;

	@Json(name = "grade9geapplicants3")
	private String grade9geapplicants3;

	@Json(name = "prgdesc4")
	private String prgdesc4;

	@Json(name = "code4")
	private String code4;

	@Json(name = "eligibility5")
	private String eligibility5;

	@Json(name = "prgdesc5")
	private String prgdesc5;

	@Json(name = "code3")
	private String code3;

	@Json(name = "prgdesc3")
	private String prgdesc3;

	@Json(name = "code6")
	private String code6;

	@Json(name = "eligibility3")
	private String eligibility3;

	@Json(name = "code5")
	private String code5;

	@Json(name = "eligibility4")
	private String eligibility4;

	@Json(name = "program4")
	private String program4;

	@Json(name = "program5")
	private String program5;

	@Json(name = "program6")
	private String program6;

	@Json(name = "grade9swdapplicants4")
	private String grade9swdapplicants4;

	@Json(name = "grade9swdapplicants3")
	private String grade9swdapplicants3;

	@Json(name = "grade9swdapplicants6")
	private String grade9swdapplicants6;

	@Json(name = "grade9swdapplicants5")
	private String grade9swdapplicants5;

	@Json(name = "seats9ge4")
	private String seats9ge4;

	@Json(name = "seats9ge3")
	private String seats9ge3;

	@Json(name = "program3")
	private String program3;

	@Json(name = "seats9ge6")
	private String seats9ge6;

	@Json(name = "seats9ge5")
	private String seats9ge5;

	@Json(name = "method5")
	private String method5;

	@Json(name = "method6")
	private String method6;

	@Json(name = "grade9gefilledflag4")
	private String grade9gefilledflag4;

	@Json(name = "grade9gefilledflag5")
	private String grade9gefilledflag5;

	@Json(name = "method3")
	private String method3;

	@Json(name = "method4")
	private String method4;

	@Json(name = "grade9gefilledflag3")
	private String grade9gefilledflag3;

	@Json(name = "grade9geapplicantsperseat5")
	private String grade9geapplicantsperseat5;

	@Json(name = "grade9geapplicantsperseat6")
	private String grade9geapplicantsperseat6;

	@Json(name = "grade9gefilledflag6")
	private String grade9gefilledflag6;

	@Json(name = "grade9geapplicantsperseat3")
	private String grade9geapplicantsperseat3;

	@Json(name = "grade9geapplicantsperseat4")
	private String grade9geapplicantsperseat4;

	@Json(name = "interest6")
	private String interest6;

	@Json(name = "interest3")
	private String interest3;

	@Json(name = "interest5")
	private String interest5;

	@Json(name = "interest4")
	private String interest4;

	@Json(name = "grade9swdapplicantsperseat3")
	private String grade9swdapplicantsperseat3;

	@Json(name = "grade9swdapplicantsperseat4")
	private String grade9swdapplicantsperseat4;

	@Json(name = "grade9swdapplicantsperseat5")
	private String grade9swdapplicantsperseat5;

	@Json(name = "grade9swdapplicantsperseat6")
	private String grade9swdapplicantsperseat6;

	@Json(name = "geoeligibility")
	private String geoeligibility;

	@Json(name = "admissionspriority22")
	private String admissionspriority22;

	@Json(name = "offer_rate2")
	private String offerRate2;

	@Json(name = "admissionspriority42")
	private String admissionspriority42;

	@Json(name = "admissionspriority32")
	private String admissionspriority32;

	@Json(name = "earlycollege")
	private String earlycollege;

	@Json(name = "ptech")
	private String ptech;

	@Json(name = "transfer")
	private String transfer;

	@Json(name = "requirement2_3")
	private String requirement23;

	@Json(name = "requirement2_4")
	private String requirement24;

	@Json(name = "requirement2_5")
	private String requirement25;

	@Json(name = "requirement3_3")
	private String requirement33;

	@Json(name = "requirement3_4")
	private String requirement34;

	@Json(name = "requirement3_5")
	private String requirement35;

	@Json(name = "auditioninformation4")
	private String auditioninformation4;

	@Json(name = "requirement4_4")
	private String requirement44;

	@Json(name = "requirement4_5")
	private String requirement45;

	@Json(name = "requirement1_2")
	private String requirement12;

	@Json(name = "requirement1_3")
	private String requirement13;

	@Json(name = "requirement1_4")
	private String requirement14;

	@Json(name = "requirement1_5")
	private String requirement15;

	@Json(name = "requirement5_4")
	private String requirement54;

	@Json(name = "requirement2_2")
	private String requirement22;

	@Json(name = "requirement3_2")
	private String requirement32;

	@Json(name = "seats9swd7")
	private String seats9swd7;

	@Json(name = "seats107")
	private String seats107;

	@Json(name = "grade9swdfilledflag7")
	private String grade9swdfilledflag7;

	@Json(name = "admissionspriority15")
	private String admissionspriority15;

	@Json(name = "grade9geapplicants7")
	private String grade9geapplicants7;

	@Json(name = "admissionspriority14")
	private String admissionspriority14;

	@Json(name = "requirement2_6")
	private String requirement26;

	@Json(name = "admissionspriority17")
	private String admissionspriority17;

	@Json(name = "prgdesc7")
	private String prgdesc7;

	@Json(name = "requirement2_7")
	private String requirement27;

	@Json(name = "code7")
	private String code7;

	@Json(name = "program7")
	private String program7;

	@Json(name = "requirement3_7")
	private String requirement37;

	@Json(name = "seats9ge7")
	private String seats9ge7;

	@Json(name = "grade9swdapplicants7")
	private String grade9swdapplicants7;

	@Json(name = "requirement3_6")
	private String requirement36;

	@Json(name = "method7")
	private String method7;

	@Json(name = "grade9geapplicantsperseat7")
	private String grade9geapplicantsperseat7;

	@Json(name = "grade9gefilledflag7")
	private String grade9gefilledflag7;

	@Json(name = "interest7")
	private String interest7;

	@Json(name = "grade9swdapplicantsperseat7")
	private String grade9swdapplicantsperseat7;

	@Json(name = "requirement1_6")
	private String requirement16;

	@Json(name = "requirement1_7")
	private String requirement17;

	@Json(name = "grade9swdfilledflag9")
	private String grade9swdfilledflag9;

	@Json(name = "grade9swdfilledflag8")
	private String grade9swdfilledflag8;

	@Json(name = "prgdesc8")
	private String prgdesc8;

	@Json(name = "prgdesc9")
	private String prgdesc9;

	@Json(name = "code9")
	private String code9;

	@Json(name = "code8")
	private String code8;

	@Json(name = "program8")
	private String program8;

	@Json(name = "program9")
	private String program9;

	@Json(name = "seats9ge9")
	private String seats9ge9;

	@Json(name = "seats9ge8")
	private String seats9ge8;

	@Json(name = "grade9swdapplicants8")
	private String grade9swdapplicants8;

	@Json(name = "grade9swdapplicants9")
	private String grade9swdapplicants9;

	@Json(name = "grade9geapplicantsperseat9")
	private String grade9geapplicantsperseat9;

	@Json(name = "grade9gefilledflag8")
	private String grade9gefilledflag8;

	@Json(name = "grade9gefilledflag9")
	private String grade9gefilledflag9;

	@Json(name = "grade9geapplicantsperseat8")
	private String grade9geapplicantsperseat8;

	@Json(name = "grade9swdapplicantsperseat8")
	private String grade9swdapplicantsperseat8;

	@Json(name = "grade9swdapplicantsperseat9")
	private String grade9swdapplicantsperseat9;

	@Json(name = "admissionspriority26")
	private String admissionspriority26;

	@Json(name = "admissionspriority27")
	private String admissionspriority27;

	@Json(name = "seats9swd8")
	private String seats9swd8;

	@Json(name = "admissionspriority24")
	private String admissionspriority24;

	@Json(name = "admissionspriority25")
	private String admissionspriority25;

	@Json(name = "seats9swd9")
	private String seats9swd9;

	@Json(name = "admissionspriority28")
	private String admissionspriority28;

	@Json(name = "offer_rate7")
	private String offerRate7;

	@Json(name = "offer_rate8")
	private String offerRate8;

	@Json(name = "offer_rate5")
	private String offerRate5;

	@Json(name = "offer_rate6")
	private String offerRate6;

	@Json(name = "offer_rate4")
	private String offerRate4;

	@Json(name = "seats108")
	private String seats108;

	@Json(name = "grade9geapplicants9")
	private String grade9geapplicants9;

	@Json(name = "seats109")
	private String seats109;

	@Json(name = "grade9geapplicants8")
	private String grade9geapplicants8;

	@Json(name = "admissionspriority19")
	private String admissionspriority19;

	@Json(name = "admissionspriority18")
	private String admissionspriority18;

	@Json(name = "method8")
	private String method8;

	@Json(name = "method9")
	private String method9;

	@Json(name = "interest9")
	private String interest9;

	@Json(name = "interest8")
	private String interest8;

	@Json(name = "requirement1_8")
	private String requirement18;

	@Json(name = "requirement6_7")
	private String requirement67;

	@Json(name = "requirement4_7")
	private String requirement47;

	@Json(name = "requirement5_7")
	private String requirement57;

	@Json(name = "auditioninformation7")
	private String auditioninformation7;

	@Json(name = "auditioninformation6")
	private String auditioninformation6;

	@Json(name = "auditioninformation5")
	private String auditioninformation5;

	@Json(name = "requirement4_3")
	private String requirement43;

	@Json(name = "girls")
	private String girls;

	@Json(name = "admissionspriority62")
	private String admissionspriority62;

	@Json(name = "admissionspriority63")
	private String admissionspriority63;

	@Json(name = "admissionspriority61")
	private String admissionspriority61;

	@Json(name = "admissionspriority52")
	private String admissionspriority52;

	@Json(name = "admissionspriority53")
	private String admissionspriority53;

	@Json(name = "admissionspriority43")
	private String admissionspriority43;

	@Json(name = "admissionspriority33")
	private String admissionspriority33;

	@Json(name = "applicants1specialized")
	private String applicants1specialized;

	@Json(name = "specialized")
	private String specialized;

	@Json(name = "seats1specialized")
	private String seats1specialized;

	@Json(name = "appperseat1specialized")
	private String appperseat1specialized;

	@Json(name = "auditioninformation3")
	private String auditioninformation3;

	@Json(name = "auditioninformation2")
	private String auditioninformation2;

	@Json(name = "auditioninformation1")
	private String auditioninformation1;

	@Json(name = "requirement4_2")
	private String requirement42;

	@Json(name = "admissionspriority34")
	private String admissionspriority34;

	@Json(name = "requirement5_5")
	private String requirement55;

	@Json(name = "admissionspriority35")
	private String admissionspriority35;

	@Json(name = "requirement5_2")
	private String requirement52;

	@Json(name = "requirement5_3")
	private String requirement53;

	@Json(name = "admissionspriority71")
	private String admissionspriority71;

	@Json(name = "common_audition1")
	private String commonAudition1;

	@Json(name = "common_audition2")
	private String commonAudition2;

	@Json(name = "common_audition3")
	private String commonAudition3;

	@Json(name = "common_audition4")
	private String commonAudition4;

	@Json(name = "common_audition5")
	private String commonAudition5;

	@Json(name = "directions2")
	private String directions2;

	@Json(name = "seats3specialized")
	private String seats3specialized;

	@Json(name = "applicants5specialized")
	private String applicants5specialized;

	@Json(name = "appperseat5specialized")
	private String appperseat5specialized;

	@Json(name = "applicants6specialized")
	private String applicants6specialized;

	@Json(name = "appperseat4specialized")
	private String appperseat4specialized;

	@Json(name = "seats6specialized")
	private String seats6specialized;

	@Json(name = "seats4specialized")
	private String seats4specialized;

	@Json(name = "directions4")
	private String directions4;

	@Json(name = "directions5")
	private String directions5;

	@Json(name = "directions6")
	private String directions6;

	@Json(name = "directions3")
	private String directions3;

	@Json(name = "appperseat2specialized")
	private String appperseat2specialized;

	@Json(name = "seats2specialized")
	private String seats2specialized;

	@Json(name = "appperseat3specialized")
	private String appperseat3specialized;

	@Json(name = "appperseat6specialized")
	private String appperseat6specialized;

	@Json(name = "applicants4specialized")
	private String applicants4specialized;

	@Json(name = "seats5specialized")
	private String seats5specialized;

	@Json(name = "requirement4_6")
	private String requirement46;

	@Json(name = "applicants3specialized")
	private String applicants3specialized;

	@Json(name = "common_audition6")
	private String commonAudition6;

	@Json(name = "requirement5_6")
	private String requirement56;

	@Json(name = "applicants2specialized")
	private String applicants2specialized;

	@Json(name = "admissionspriority44")
	private String admissionspriority44;

	@Json(name = "eligibility6")
	private String eligibility6;

	@Json(name = "prgdesc10")
	private String prgdesc10;

	@Json(name = "admissionspriority110")
	private String admissionspriority110;

	@Json(name = "grade9gefilledflag10")
	private String grade9gefilledflag10;

	@Json(name = "interest10")
	private String interest10;

	@Json(name = "seats9swd10")
	private String seats9swd10;

	@Json(name = "grade9swdfilledflag10")
	private String grade9swdfilledflag10;

	@Json(name = "seats1010")
	private String seats1010;

	@Json(name = "grade9swdapplicantsperseat10")
	private String grade9swdapplicantsperseat10;

	@Json(name = "grade9geapplicants10")
	private String grade9geapplicants10;

	@Json(name = "seats9ge10")
	private String seats9ge10;

	@Json(name = "grade9geapplicantsperseat10")
	private String grade9geapplicantsperseat10;

	@Json(name = "admissionspriority29")
	private String admissionspriority29;

	@Json(name = "offer_rate9")
	private String offerRate9;

	@Json(name = "requirement2_8")
	private String requirement28;

	@Json(name = "method10")
	private String method10;

	@Json(name = "requirement3_8")
	private String requirement38;

	@Json(name = "grade9swdapplicants10")
	private String grade9swdapplicants10;

	@Json(name = "program10")
	private String program10;

	@Json(name = "code10")
	private String code10;

	@Json(name = "eligibility7")
	private String eligibility7;

	@Json(name = "common_audition7")
	private String commonAudition7;

	@Json(name = "requirement6_2")
	private String requirement62;

	@Json(name = "admissionspriority74")
	private String admissionspriority74;

	@Json(name = "admissionspriority64")
	private String admissionspriority64;

	@Json(name = "admissionspriority54")
	private String admissionspriority54;

	@Json(name = "admissionspriority37")
	private String admissionspriority37;

	@Json(name = "admissionspriority56")
	private String admissionspriority56;

	@Json(name = "admissionspriority46")
	private String admissionspriority46;

	@Json(name = "admissionspriority36")
	private String admissionspriority36;

	@Json(name = "requirement6_3")
	private String requirement63;

	@Json(name = "directions7")
	private String directions7;

	public String getBus(){
		return bus;
	}

	public String getGrade9gefilledflag1(){
		return grade9gefilledflag1;
	}

	public String getMethod1(){
		return method1;
	}

	public String getMethod2(){
		return method2;
	}

	public String getGrade9gefilledflag2(){
		return grade9gefilledflag2;
	}

	public String getBin(){
		return bin;
	}

	public String getBorough(){
		return borough;
	}

	public String getGrade9geapplicantsperseat1(){
		return grade9geapplicantsperseat1;
	}

	public String getGrade9geapplicantsperseat2(){
		return grade9geapplicantsperseat2;
	}

	public String getEllPrograms(){
		return ellPrograms;
	}

	public String getDiplomaendorsements(){
		return diplomaendorsements;
	}

	public String getPsalSportsGirls(){
		return psalSportsGirls;
	}

	public String getAcademicopportunities1(){
		return academicopportunities1;
	}

	public String getAcademicopportunities2(){
		return academicopportunities2;
	}

	public String getAcademicopportunities3(){
		return academicopportunities3;
	}

	public String getAcademicopportunities4(){
		return academicopportunities4;
	}

	public String getAcademicopportunities5(){
		return academicopportunities5;
	}

	public String getTotalStudents(){
		return totalStudents;
	}

	public String getPsalSportsCoed(){
		return psalSportsCoed;
	}

	public String getLongitude(){
		return longitude;
	}

	public String getZip(){
		return zip;
	}

	public String getFinalgrades(){
		return finalgrades;
	}

	public String getSeats9swd2(){
		return seats9swd2;
	}

	public String getAdmissionspriority11(){
		return admissionspriority11;
	}

	public String getSeats9swd1(){
		return seats9swd1;
	}

	public String getAdmissionspriority12(){
		return admissionspriority12;
	}

	public String getFaxNumber(){
		return faxNumber;
	}

	public String getBbl(){
		return bbl;
	}

	public String getDbn(){
		return dbn;
	}

	public String getStartTime(){
		return startTime;
	}

	public String getPctStuSafe(){
		return pctStuSafe;
	}

	public String getPctStuEnoughVariety(){
		return pctStuEnoughVariety;
	}

	public String getInterest1(){
		return interest1;
	}

	public String getOverviewParagraph(){
		return overviewParagraph;
	}

	public String getSchoolAccessibilityDescription(){
		return schoolAccessibilityDescription;
	}

	public String getCommunityBoard(){
		return communityBoard;
	}

	public String getLanguageClasses(){
		return languageClasses;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public String getNeighborhood(){
		return neighborhood;
	}

	public String getCampusName(){
		return campusName;
	}

	public String getGrade9geapplicants2(){
		return grade9geapplicants2;
	}

	public String getGrade9swdfilledflag1(){
		return grade9swdfilledflag1;
	}

	public String getStateCode(){
		return stateCode;
	}

	public String getCouncilDistrict(){
		return councilDistrict;
	}

	public String getInterest2(){
		return interest2;
	}

	public String getGrade9geapplicants1(){
		return grade9geapplicants1;
	}

	public String getSeats101(){
		return seats101;
	}

	public String getGrade9swdapplicantsperseat1(){
		return grade9swdapplicantsperseat1;
	}

	public String getGrade9swdfilledflag2(){
		return grade9swdfilledflag2;
	}

	public String getSeats102(){
		return seats102;
	}

	public String getGrade9swdapplicantsperseat2(){
		return grade9swdapplicantsperseat2;
	}

	public String getSharedSpace(){
		return sharedSpace;
	}

	public String getPrgdesc2(){
		return prgdesc2;
	}

	public String getCode2(){
		return code2;
	}

	public String getCode1(){
		return code1;
	}

	public String getPrgdesc1(){
		return prgdesc1;
	}

	public String getCity(){
		return city;
	}

	public String getLatitude(){
		return latitude;
	}

	public String getBuildingCode(){
		return buildingCode;
	}

	public String getGraduationRate(){
		return graduationRate;
	}

	public String getNta(){
		return nta;
	}

	public String getExtracurricularActivities(){
		return extracurricularActivities;
	}

	public String getCensusTract(){
		return censusTract;
	}

	public String getWebsite(){
		return website;
	}

	public String getBoro(){
		return boro;
	}

	public String getEndTime(){
		return endTime;
	}

	public String getSchoolName(){
		return schoolName;
	}

	public String getGrades2018(){
		return grades2018;
	}

	public String getPrimaryAddressLine1(){
		return primaryAddressLine1;
	}

	public String getPsalSportsBoys(){
		return psalSportsBoys;
	}

	public String getSchoolEmail(){
		return schoolEmail;
	}

	public String getSubway(){
		return subway;
	}

	public String getSchool10thSeats(){
		return school10thSeats;
	}

	public String getGrade9swdapplicants2(){
		return grade9swdapplicants2;
	}

	public String getGrade9swdapplicants1(){
		return grade9swdapplicants1;
	}

	public String getAttendanceRate(){
		return attendanceRate;
	}

	public String getProgram1(){
		return program1;
	}

	public String getLocation(){
		return location;
	}

	public String getProgram2(){
		return program2;
	}

	public String getSeats9ge2(){
		return seats9ge2;
	}

	public String getSeats9ge1(){
		return seats9ge1;
	}

	public String getAdvancedplacementCourses(){
		return advancedplacementCourses;
	}

	public String getCollegeCareerRate(){
		return collegeCareerRate;
	}

	public String getAdmissionspriority21(){
		return admissionspriority21;
	}

	public String getAddtlInfo1(){
		return addtlInfo1;
	}

	public String getAdmissionspriority51(){
		return admissionspriority51;
	}

	public String getSchoolSports(){
		return schoolSports;
	}

	public String getOfferRate1(){
		return offerRate1;
	}

	public String getAdmissionspriority41(){
		return admissionspriority41;
	}

	public String getAdmissionspriority31(){
		return admissionspriority31;
	}

	public String getPbat(){
		return pbat;
	}

	public String getInternational(){
		return international;
	}

	public String getEligibility1(){
		return eligibility1;
	}

	public String getEligibility2(){
		return eligibility2;
	}

	public String getRequirement21(){
		return requirement21;
	}

	public String getRequirement41(){
		return requirement41;
	}

	public String getRequirement61(){
		return requirement61;
	}

	public String getDirections1(){
		return directions1;
	}

	public String getRequirement11(){
		return requirement11;
	}

	public String getRequirement31(){
		return requirement31;
	}

	public String getRequirement51(){
		return requirement51;
	}

	public String getBoys(){
		return boys;
	}

	public String getAdmissionspriority23(){
		return admissionspriority23;
	}

	public String getSeats9swd6(){
		return seats9swd6;
	}

	public String getSeats9swd5(){
		return seats9swd5;
	}

	public String getSeats9swd4(){
		return seats9swd4;
	}

	public String getSeats9swd3(){
		return seats9swd3;
	}

	public String getOfferRate3(){
		return offerRate3;
	}

	public String getGrade9swdfilledflag5(){
		return grade9swdfilledflag5;
	}

	public String getGrade9swdfilledflag4(){
		return grade9swdfilledflag4;
	}

	public String getSeats105(){
		return seats105;
	}

	public String getGrade9swdfilledflag6(){
		return grade9swdfilledflag6;
	}

	public String getSeats106(){
		return seats106;
	}

	public String getGrade9geapplicants6(){
		return grade9geapplicants6;
	}

	public String getGrade9geapplicants5(){
		return grade9geapplicants5;
	}

	public String getAdmissionspriority16(){
		return admissionspriority16;
	}

	public String getAdmissionspriority13(){
		return admissionspriority13;
	}

	public String getSeats103(){
		return seats103;
	}

	public String getSeats104(){
		return seats104;
	}

	public String getPrgdesc6(){
		return prgdesc6;
	}

	public String getGrade9geapplicants4(){
		return grade9geapplicants4;
	}

	public String getGrade9swdfilledflag3(){
		return grade9swdfilledflag3;
	}

	public String getGrade9geapplicants3(){
		return grade9geapplicants3;
	}

	public String getPrgdesc4(){
		return prgdesc4;
	}

	public String getCode4(){
		return code4;
	}

	public String getEligibility5(){
		return eligibility5;
	}

	public String getPrgdesc5(){
		return prgdesc5;
	}

	public String getCode3(){
		return code3;
	}

	public String getPrgdesc3(){
		return prgdesc3;
	}

	public String getCode6(){
		return code6;
	}

	public String getEligibility3(){
		return eligibility3;
	}

	public String getCode5(){
		return code5;
	}

	public String getEligibility4(){
		return eligibility4;
	}

	public String getProgram4(){
		return program4;
	}

	public String getProgram5(){
		return program5;
	}

	public String getProgram6(){
		return program6;
	}

	public String getGrade9swdapplicants4(){
		return grade9swdapplicants4;
	}

	public String getGrade9swdapplicants3(){
		return grade9swdapplicants3;
	}

	public String getGrade9swdapplicants6(){
		return grade9swdapplicants6;
	}

	public String getGrade9swdapplicants5(){
		return grade9swdapplicants5;
	}

	public String getSeats9ge4(){
		return seats9ge4;
	}

	public String getSeats9ge3(){
		return seats9ge3;
	}

	public String getProgram3(){
		return program3;
	}

	public String getSeats9ge6(){
		return seats9ge6;
	}

	public String getSeats9ge5(){
		return seats9ge5;
	}

	public String getMethod5(){
		return method5;
	}

	public String getMethod6(){
		return method6;
	}

	public String getGrade9gefilledflag4(){
		return grade9gefilledflag4;
	}

	public String getGrade9gefilledflag5(){
		return grade9gefilledflag5;
	}

	public String getMethod3(){
		return method3;
	}

	public String getMethod4(){
		return method4;
	}

	public String getGrade9gefilledflag3(){
		return grade9gefilledflag3;
	}

	public String getGrade9geapplicantsperseat5(){
		return grade9geapplicantsperseat5;
	}

	public String getGrade9geapplicantsperseat6(){
		return grade9geapplicantsperseat6;
	}

	public String getGrade9gefilledflag6(){
		return grade9gefilledflag6;
	}

	public String getGrade9geapplicantsperseat3(){
		return grade9geapplicantsperseat3;
	}

	public String getGrade9geapplicantsperseat4(){
		return grade9geapplicantsperseat4;
	}

	public String getInterest6(){
		return interest6;
	}

	public String getInterest3(){
		return interest3;
	}

	public String getInterest5(){
		return interest5;
	}

	public String getInterest4(){
		return interest4;
	}

	public String getGrade9swdapplicantsperseat3(){
		return grade9swdapplicantsperseat3;
	}

	public String getGrade9swdapplicantsperseat4(){
		return grade9swdapplicantsperseat4;
	}

	public String getGrade9swdapplicantsperseat5(){
		return grade9swdapplicantsperseat5;
	}

	public String getGrade9swdapplicantsperseat6(){
		return grade9swdapplicantsperseat6;
	}

	public String getGeoeligibility(){
		return geoeligibility;
	}

	public String getAdmissionspriority22(){
		return admissionspriority22;
	}

	public String getOfferRate2(){
		return offerRate2;
	}

	public String getAdmissionspriority42(){
		return admissionspriority42;
	}

	public String getAdmissionspriority32(){
		return admissionspriority32;
	}

	public String getEarlycollege(){
		return earlycollege;
	}

	public String getPtech(){
		return ptech;
	}

	public String getTransfer(){
		return transfer;
	}

	public String getRequirement23(){
		return requirement23;
	}

	public String getRequirement24(){
		return requirement24;
	}

	public String getRequirement25(){
		return requirement25;
	}

	public String getRequirement33(){
		return requirement33;
	}

	public String getRequirement34(){
		return requirement34;
	}

	public String getRequirement35(){
		return requirement35;
	}

	public String getAuditioninformation4(){
		return auditioninformation4;
	}

	public String getRequirement44(){
		return requirement44;
	}

	public String getRequirement45(){
		return requirement45;
	}

	public String getRequirement12(){
		return requirement12;
	}

	public String getRequirement13(){
		return requirement13;
	}

	public String getRequirement14(){
		return requirement14;
	}

	public String getRequirement15(){
		return requirement15;
	}

	public String getRequirement54(){
		return requirement54;
	}

	public String getRequirement22(){
		return requirement22;
	}

	public String getRequirement32(){
		return requirement32;
	}

	public String getSeats9swd7(){
		return seats9swd7;
	}

	public String getSeats107(){
		return seats107;
	}

	public String getGrade9swdfilledflag7(){
		return grade9swdfilledflag7;
	}

	public String getAdmissionspriority15(){
		return admissionspriority15;
	}

	public String getGrade9geapplicants7(){
		return grade9geapplicants7;
	}

	public String getAdmissionspriority14(){
		return admissionspriority14;
	}

	public String getRequirement26(){
		return requirement26;
	}

	public String getAdmissionspriority17(){
		return admissionspriority17;
	}

	public String getPrgdesc7(){
		return prgdesc7;
	}

	public String getRequirement27(){
		return requirement27;
	}

	public String getCode7(){
		return code7;
	}

	public String getProgram7(){
		return program7;
	}

	public String getRequirement37(){
		return requirement37;
	}

	public String getSeats9ge7(){
		return seats9ge7;
	}

	public String getGrade9swdapplicants7(){
		return grade9swdapplicants7;
	}

	public String getRequirement36(){
		return requirement36;
	}

	public String getMethod7(){
		return method7;
	}

	public String getGrade9geapplicantsperseat7(){
		return grade9geapplicantsperseat7;
	}

	public String getGrade9gefilledflag7(){
		return grade9gefilledflag7;
	}

	public String getInterest7(){
		return interest7;
	}

	public String getGrade9swdapplicantsperseat7(){
		return grade9swdapplicantsperseat7;
	}

	public String getRequirement16(){
		return requirement16;
	}

	public String getRequirement17(){
		return requirement17;
	}

	public String getGrade9swdfilledflag9(){
		return grade9swdfilledflag9;
	}

	public String getGrade9swdfilledflag8(){
		return grade9swdfilledflag8;
	}

	public String getPrgdesc8(){
		return prgdesc8;
	}

	public String getPrgdesc9(){
		return prgdesc9;
	}

	public String getCode9(){
		return code9;
	}

	public String getCode8(){
		return code8;
	}

	public String getProgram8(){
		return program8;
	}

	public String getProgram9(){
		return program9;
	}

	public String getSeats9ge9(){
		return seats9ge9;
	}

	public String getSeats9ge8(){
		return seats9ge8;
	}

	public String getGrade9swdapplicants8(){
		return grade9swdapplicants8;
	}

	public String getGrade9swdapplicants9(){
		return grade9swdapplicants9;
	}

	public String getGrade9geapplicantsperseat9(){
		return grade9geapplicantsperseat9;
	}

	public String getGrade9gefilledflag8(){
		return grade9gefilledflag8;
	}

	public String getGrade9gefilledflag9(){
		return grade9gefilledflag9;
	}

	public String getGrade9geapplicantsperseat8(){
		return grade9geapplicantsperseat8;
	}

	public String getGrade9swdapplicantsperseat8(){
		return grade9swdapplicantsperseat8;
	}

	public String getGrade9swdapplicantsperseat9(){
		return grade9swdapplicantsperseat9;
	}

	public String getAdmissionspriority26(){
		return admissionspriority26;
	}

	public String getAdmissionspriority27(){
		return admissionspriority27;
	}

	public String getSeats9swd8(){
		return seats9swd8;
	}

	public String getAdmissionspriority24(){
		return admissionspriority24;
	}

	public String getAdmissionspriority25(){
		return admissionspriority25;
	}

	public String getSeats9swd9(){
		return seats9swd9;
	}

	public String getAdmissionspriority28(){
		return admissionspriority28;
	}

	public String getOfferRate7(){
		return offerRate7;
	}

	public String getOfferRate8(){
		return offerRate8;
	}

	public String getOfferRate5(){
		return offerRate5;
	}

	public String getOfferRate6(){
		return offerRate6;
	}

	public String getOfferRate4(){
		return offerRate4;
	}

	public String getSeats108(){
		return seats108;
	}

	public String getGrade9geapplicants9(){
		return grade9geapplicants9;
	}

	public String getSeats109(){
		return seats109;
	}

	public String getGrade9geapplicants8(){
		return grade9geapplicants8;
	}

	public String getAdmissionspriority19(){
		return admissionspriority19;
	}

	public String getAdmissionspriority18(){
		return admissionspriority18;
	}

	public String getMethod8(){
		return method8;
	}

	public String getMethod9(){
		return method9;
	}

	public String getInterest9(){
		return interest9;
	}

	public String getInterest8(){
		return interest8;
	}

	public String getRequirement18(){
		return requirement18;
	}

	public String getRequirement67(){
		return requirement67;
	}

	public String getRequirement47(){
		return requirement47;
	}

	public String getRequirement57(){
		return requirement57;
	}

	public String getAuditioninformation7(){
		return auditioninformation7;
	}

	public String getAuditioninformation6(){
		return auditioninformation6;
	}

	public String getAuditioninformation5(){
		return auditioninformation5;
	}

	public String getRequirement43(){
		return requirement43;
	}

	public String getGirls(){
		return girls;
	}

	public String getAdmissionspriority62(){
		return admissionspriority62;
	}

	public String getAdmissionspriority63(){
		return admissionspriority63;
	}

	public String getAdmissionspriority61(){
		return admissionspriority61;
	}

	public String getAdmissionspriority52(){
		return admissionspriority52;
	}

	public String getAdmissionspriority53(){
		return admissionspriority53;
	}

	public String getAdmissionspriority43(){
		return admissionspriority43;
	}

	public String getAdmissionspriority33(){
		return admissionspriority33;
	}

	public String getApplicants1specialized(){
		return applicants1specialized;
	}

	public String getSpecialized(){
		return specialized;
	}

	public String getSeats1specialized(){
		return seats1specialized;
	}

	public String getAppperseat1specialized(){
		return appperseat1specialized;
	}

	public String getAuditioninformation3(){
		return auditioninformation3;
	}

	public String getAuditioninformation2(){
		return auditioninformation2;
	}

	public String getAuditioninformation1(){
		return auditioninformation1;
	}

	public String getRequirement42(){
		return requirement42;
	}

	public String getAdmissionspriority34(){
		return admissionspriority34;
	}

	public String getRequirement55(){
		return requirement55;
	}

	public String getAdmissionspriority35(){
		return admissionspriority35;
	}

	public String getRequirement52(){
		return requirement52;
	}

	public String getRequirement53(){
		return requirement53;
	}

	public String getAdmissionspriority71(){
		return admissionspriority71;
	}

	public String getCommonAudition1(){
		return commonAudition1;
	}

	public String getCommonAudition2(){
		return commonAudition2;
	}

	public String getCommonAudition3(){
		return commonAudition3;
	}

	public String getCommonAudition4(){
		return commonAudition4;
	}

	public String getCommonAudition5(){
		return commonAudition5;
	}

	public String getDirections2(){
		return directions2;
	}

	public String getSeats3specialized(){
		return seats3specialized;
	}

	public String getApplicants5specialized(){
		return applicants5specialized;
	}

	public String getAppperseat5specialized(){
		return appperseat5specialized;
	}

	public String getApplicants6specialized(){
		return applicants6specialized;
	}

	public String getAppperseat4specialized(){
		return appperseat4specialized;
	}

	public String getSeats6specialized(){
		return seats6specialized;
	}

	public String getSeats4specialized(){
		return seats4specialized;
	}

	public String getDirections4(){
		return directions4;
	}

	public String getDirections5(){
		return directions5;
	}

	public String getDirections6(){
		return directions6;
	}

	public String getDirections3(){
		return directions3;
	}

	public String getAppperseat2specialized(){
		return appperseat2specialized;
	}

	public String getSeats2specialized(){
		return seats2specialized;
	}

	public String getAppperseat3specialized(){
		return appperseat3specialized;
	}

	public String getAppperseat6specialized(){
		return appperseat6specialized;
	}

	public String getApplicants4specialized(){
		return applicants4specialized;
	}

	public String getSeats5specialized(){
		return seats5specialized;
	}

	public String getRequirement46(){
		return requirement46;
	}

	public String getApplicants3specialized(){
		return applicants3specialized;
	}

	public String getCommonAudition6(){
		return commonAudition6;
	}

	public String getRequirement56(){
		return requirement56;
	}

	public String getApplicants2specialized(){
		return applicants2specialized;
	}

	public String getAdmissionspriority44(){
		return admissionspriority44;
	}

	public String getEligibility6(){
		return eligibility6;
	}

	public String getPrgdesc10(){
		return prgdesc10;
	}

	public String getAdmissionspriority110(){
		return admissionspriority110;
	}

	public String getGrade9gefilledflag10(){
		return grade9gefilledflag10;
	}

	public String getInterest10(){
		return interest10;
	}

	public String getSeats9swd10(){
		return seats9swd10;
	}

	public String getGrade9swdfilledflag10(){
		return grade9swdfilledflag10;
	}

	public String getSeats1010(){
		return seats1010;
	}

	public String getGrade9swdapplicantsperseat10(){
		return grade9swdapplicantsperseat10;
	}

	public String getGrade9geapplicants10(){
		return grade9geapplicants10;
	}

	public String getSeats9ge10(){
		return seats9ge10;
	}

	public String getGrade9geapplicantsperseat10(){
		return grade9geapplicantsperseat10;
	}

	public String getAdmissionspriority29(){
		return admissionspriority29;
	}

	public String getOfferRate9(){
		return offerRate9;
	}

	public String getRequirement28(){
		return requirement28;
	}

	public String getMethod10(){
		return method10;
	}

	public String getRequirement38(){
		return requirement38;
	}

	public String getGrade9swdapplicants10(){
		return grade9swdapplicants10;
	}

	public String getProgram10(){
		return program10;
	}

	public String getCode10(){
		return code10;
	}

	public String getEligibility7(){
		return eligibility7;
	}

	public String getCommonAudition7(){
		return commonAudition7;
	}

	public String getRequirement62(){
		return requirement62;
	}

	public String getAdmissionspriority74(){
		return admissionspriority74;
	}

	public String getAdmissionspriority64(){
		return admissionspriority64;
	}

	public String getAdmissionspriority54(){
		return admissionspriority54;
	}

	public String getAdmissionspriority37(){
		return admissionspriority37;
	}

	public String getAdmissionspriority56(){
		return admissionspriority56;
	}

	public String getAdmissionspriority46(){
		return admissionspriority46;
	}

	public String getAdmissionspriority36(){
		return admissionspriority36;
	}

	public String getRequirement63(){
		return requirement63;
	}

	public String getDirections7(){
		return directions7;
	}
}