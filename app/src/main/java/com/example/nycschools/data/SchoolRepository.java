package com.example.nycschools.data;

import androidx.lifecycle.MutableLiveData;

import com.example.nycschools.model.SchoolScoreInfo;
import com.example.nycschools.model.SchoolInfo;
import com.example.nycschools.network.ApiService;
import com.example.nycschools.network.ErrorHandler;
import com.example.nycschools.network.RetrofitClient;
import com.example.nycschools.utils.GlobalVariables;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolRepository {

    private static SchoolRepository instance;
    private ApiService mApiService;
    private MutableLiveData<List<SchoolInfo>> mListOfSchoolInfoMutableLiveData;
    private MutableLiveData<List<SchoolScoreInfo>> mListOfSchoolScoreInfoMutableLiveData;

    public static SchoolRepository getInstance() {
        if (instance == null) {
            instance = new SchoolRepository();
        }
        return instance;
    }

    private SchoolRepository() {
        mListOfSchoolInfoMutableLiveData = new MutableLiveData<>();
        mListOfSchoolScoreInfoMutableLiveData = new MutableLiveData<>();
        mApiService = RetrofitClient.getInstance().getApiService();

    }

    private MutableLiveData<List<SchoolInfo>> getListOfSchoolInfoFromServer() {
        mApiService.getListOfNYCSchools().enqueue(new Callback<List<SchoolInfo>>() {
            @Override
            public void onResponse(Call<List<SchoolInfo>> call, Response<List<SchoolInfo>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mListOfSchoolInfoMutableLiveData.postValue(response.body());
                    return;
                }


                try {
                    if (response.errorBody() != null) {
                        GlobalVariables.toastMessage.postValue(response.errorBody().toString());
                    } else {
                        GlobalVariables.toastMessage.postValue("Unable to fetch the data");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<List<SchoolInfo>> call, Throwable t) {
                ErrorHandler.handleRetrofitError(t);
                mListOfSchoolInfoMutableLiveData.postValue(null);

            }
        });
        return mListOfSchoolInfoMutableLiveData;

    }

    private MutableLiveData<List<SchoolScoreInfo>> getListOfSchoolScoreInfoFromServer() {
        mApiService.getDataOfSchools().enqueue(new Callback<List<SchoolScoreInfo>>() {
            @Override
            public void onResponse(Call<List<SchoolScoreInfo>> call, Response<List<SchoolScoreInfo>> response) {
                if (response.isSuccessful() && response.body() != null) {
                    mListOfSchoolScoreInfoMutableLiveData.postValue(response.body());
                    return;
                }

            }

            @Override
            public void onFailure(Call<List<SchoolScoreInfo>> call, Throwable t) {
                ErrorHandler.handleRetrofitError(t);
                mListOfSchoolInfoMutableLiveData.postValue(null);
            }
        });

        return mListOfSchoolScoreInfoMutableLiveData;

    }


    public MutableLiveData<List<SchoolInfo>> getListOfSchoolInfo() {
        return getListOfSchoolInfoFromServer();
    }

    public MutableLiveData<List<SchoolScoreInfo>> getSATResultOfSchool() {
        return getListOfSchoolScoreInfoFromServer();
    }
}
