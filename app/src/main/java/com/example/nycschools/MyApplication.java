package com.example.nycschools;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;

import androidx.appcompat.app.AppCompatDelegate;

public class MyApplication extends Application {

    public static ConnectivityManager connectivityManager;

    @Override
    public void onCreate() {
        super.onCreate();
        connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

}
