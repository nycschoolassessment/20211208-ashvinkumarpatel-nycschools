package com.example.nycschools.utils;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import com.example.nycschools.MyApplication;


public class NetworkCheck {

    public static boolean isNetworkAvailable() {
        try {
            ConnectivityManager cm = MyApplication.connectivityManager;
            if (cm == null) return false;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                NetworkCapabilities cap = cm.getNetworkCapabilities(cm.getActiveNetwork());
                if (cap == null) return false;
                return cap.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET);
            } else {
                Network[] networks = cm.getAllNetworks();
                for (Network n : networks) {
                    NetworkInfo nInfo = cm.getNetworkInfo(n);
                    if (nInfo != null && nInfo.isConnectedOrConnecting()) return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
